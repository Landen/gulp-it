/*

\========= GulpIt! =========/
/ lightning-fast processing \

task: png-sprite

generates png-sprite from the specified folder
default folder - 'png-sprite'
custom folder - parameter --name

dependencies:
gulp
gulp-imagemin
imagemin-pngquant
gulp.spritesmith

*/

module.exports = function (gulp, $) {

	var cfg = $.config;

	var spriteName = cfg.argv.name || cfg.assets.sprite.png;

	return function(){
		var spriteData = gulp.src($.generatePath('images') + spriteName + '/*.*').pipe($.spritesmith({
			imgName: spriteName + '.png',
			imgPath: '../' + cfg.assets.images.src + spriteName + '.png',
			cssName: '_' + spriteName + '.' + $.extension,
			padding: 2
		}));
		spriteData.img
			.pipe($.imagemin({
				use: [$.pngquant()]
			}))
			.pipe(gulp.dest($.generatePath('images')))
		spriteData.css
			.pipe(gulp.dest($.generatePath('styles') + cfg.assets.styles.import))
	}
};