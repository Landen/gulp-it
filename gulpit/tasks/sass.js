/*

\========= GulpIt! =========/
/ lightning-fast processing \

task: sass (based on node-sass/libsass)

compiles all your styles on the first run
watches for changes (default task) and compiles only changed files

features:
- autoprefixer (always | config.json -> browsers)
- combine media queries (with --production flag)
- minify css (with --production flag)
- write sourcemaps (without --production flag)

dependencies:
gulp
gulp-sass
gulp-plumber
gulp-cached
gulp-progeny
gulp-if
gulp-sourcemaps
gulp-autoprefixer
gulp-clean-css

*/

module.exports = function (gulp, $) {

	var cfg = $.config;

	function generateIncludePaths(){
		var includes = cfg.assets.styles.include,
			result = [];

		if (Array.isArray(includes)) {
			for (var i = 0; i < includes.length; i++) {
				result.push($.generatePath('styles') + includes[i]);
			};
		} else if (typeof(includes) === 'string') {
			result.push($.generatePath('styles') + includes);
		}
		return result;
	}
	return function(){
		gulp.src($.generatePath('styles') + '**/*.scss')
			.pipe($.plumber())
			.pipe($.cached('styles'))
			.pipe($.progeny({
				regexp: /^\s*@import\s*['"]?([^'"]+)['"]?/,
				prefix: '_',
				extensionsList: ['scss'],
				multipass: [
					/@import[^;]+;/g,
					/\s*['"][^'"]+['"]\s*,?/g,
					/(?:['"])([^'"]+)/
				]
			}))
			.pipe($._if(!cfg.argv.production, $.sourcemaps.init()))
			.pipe($.sass({includePaths: generateIncludePaths()}))
			.on('error', function(err){$.displayError(err);})
			.pipe($.autoprefixer(cfg.browsers))
			.pipe($._if(cfg.argv.production, $.cleanCss({keepBreaks: true})))
			.pipe($._if(!cfg.argv.production, $.sourcemaps.write()))
			.pipe($.plumber.stop())
			.pipe(gulp.dest($.generatePath('styles', true)))
	};
};