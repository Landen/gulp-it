/*

\========= GulpIt! =========/
/ lightning-fast processing \

task: svgstore

creates inline svg sprite and injects it into your template or html files
adopted for wordpress, magento, custom and static projects

Attention! Before usage insert this code ---

<!-- inject:svg -->
<!-- endinject -->

--- into your header or global layout template (above page content)

magento: path_to_your_theme/page/html/header.phtml
wordpress: path_to_your_theme/header.php
custom: path_to_your_views/layout.twig (you can change path in config.json)

For usage use the following snippet:

<svg viewBox="0 0 100 100" class="icon this">
	<use xlink:href="#this"></use>
</svg>

dependencies:
gulp
gulp-inject
gulp-filelog
gulp-cheerio
gulp-svgmin
gulp-svgstore

*/

module.exports = function (gulp, $) {

	var cfg = $.config;

	var sprite = cfg.assets.sprite.svg,
		cms = cfg.cms;

	function getInject(files){
		var path;
		switch (cfg.type) {
			case "magento":
				path = 'app/design/frontend/' + cms.package + '/' + cms.theme + '/' + sprite.inject.folder;
				if (files) path += sprite.inject.file;
				break;
			case "wordpress":
				path = 'wp-content/themes/' + cms.theme + '/' + sprite.inject.folder;
				if (files) path += sprite.inject.file;
				break;
			case "custom":
				path = cfg.basedir + cms.views + sprite.inject.folder;
				if (files) path += sprite.inject.file;
				break;
			case "static":
				// path = '/';
				path = 'twig/include/';
				if (files) {
					// path += '**/**/*.{html,php,twig}';
					path += 'svg-sprite.twig';
				}
				break;
		}
		console.log(path);
		return path;
	}
	return function(){
		var svgs = gulp
			.src($.generatePath('images') + sprite.folder + '/*.svg')
			.pipe($.filelog())
			.pipe($.cheerio({
				run: function ($) {
					$('[fill]').removeAttr('fill');
					$('[opacity]').removeAttr('opacity');
				},
				parserOptions: {xmlMode: true}
			}))
			.pipe($.svgmin())
			.pipe($.svgstore({
				inlineSvg: true,
				viewBox: '0 0 100 100',
				xmlns: 'http://www.w3.org/2000/svg'
			}))

		function fileContents(filePath, file){
			return file.contents.toString();
		}

		return gulp
			.src(getInject(true))
			.pipe($.inject(svgs, {transform: fileContents}))
			.pipe(gulp.dest(getInject()))
	};
};