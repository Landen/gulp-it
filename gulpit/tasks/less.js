/*

\========= GulpIt! =========/
/ lightning-fast processing \

task: less

compiles all your styles on the first run
watches for changes (default task) and compiles only changed files

features:
- autoprefixer (config.json -> preferences)
- combine media queries (if production == true)
- minify css (if production == true)
- write sourcemaps (if production == false)

dependencies:
gulp
gulp-plumber
gulp-cached
gulp-progeny
gulp-filter
gulp-if
gulp-less
gulp-sourcemaps
gulp-autoprefixer
gulp-clean-css

*/

module.exports = function (gulp, $) {

	var cfg = $.config;

	return function(){
		gulp.src($.generatePath('styles') + '**/*.less')
			.pipe($.plumber())
			.pipe($.cached('styles'))
			.pipe($.progeny({
				regexp: /^\s*@import\s*['"]?([^'"]+)['"]?/,
				prefix: '_',
				extensionsList: ['less'],
				multipass: [
					/@import[^;]+;/g,
					/\s*['"][^'"]+['"]\s*,?/g,
					/(?:['"])([^'"]+)/
				]
			}))
			.pipe($.filter(['**/*.less', '!_*.less', '!**/_*.less', '!modules/**/*.less']))
			.pipe($._if(!cfg.argv.production,$.sourcemaps.init()))
			.pipe($.less())
			.on('error', function(err){$.displayError(err);})
			.pipe($.autoprefixer(cfg.browsers))
			.pipe($._if(cfg.argv.production, $.cleanCss({keepBreaks: true})))
			.pipe($._if(!cfg.argv.production,$.sourcemaps.write()))
			.pipe($.plumber.stop())
			.pipe(gulp.dest($.generatePath('styles', true)))
	};
}