/*

\========= GulpIt! =========/
/ lightning-fast processing \

task: png

this is the part of 'images' task
goes through images folder + subfolders and compresses all png images

dependencies:
gulp
gulp-imagemin
imagemin-pngquant

*/

module.exports = function (gulp, $) {

	var cfg = $.config;

	return function(){
		gulp.src($.generatePath('images') + '**/*.png')
			.pipe($.imagemin({
				progressive: true,
				use: [$.pngquant()]
			}))
			.on('error', function(err){
				$.displayError(err);
			})
			.pipe(gulp.dest($.generatePath('images')));
	}
};