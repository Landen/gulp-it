/*

\========= GulpIt! =========/
/ lightning-fast processing \

Default usage:
gulp jpg
goes through images/temp folder (including subfolders) and compresses all jpg images with jpegRecompress, using parameters: {min:65, max:75, progressive:true}

flags:
--min=integer - min quality (if less than 65 - task asks for Your confirm)
--max=integer - max quality (if less than 75 - task asks for Your confirm)
--folder=string - goes through another folder
--force - goes through all jpgs inside images folder (task asks for Your confirm)

dependencies:
gulp
yargs
gulp-filelog
gulp-if
gulp-confirm
imagemin-jpeg-recompress

*/

module.exports = function (gulp, $) {

	var cfg = $.config;

	var min = cfg.argv.min ? cfg.argv.min : 65,
		max = cfg.argv.max ? cfg.argv.max : 75,
		folder = '';

	if (!cfg.argv.force) {
		folder = cfg.argv.folder ? (cfg.argv.folder + '/') : 'temp/';
	}

	return function(){
		gulp.src($.generatePath('images') + folder + '**/*.jpg')
			.pipe($._if(cfg.argv.force, $.confirm({
				question: 'This will overwrite all Your jpg files in \'images\' folder. Continue? (y)',
				input: '_key:y'
			})))
			.pipe($._if(cfg.argv.min && cfg.argv.min < 65, $.confirm({
				question: 'Min quality you specified can greatly reduce the quality of images. Continue (y)?',
				input: '_key:y'
			})))
			.pipe($._if(cfg.argv.max && cfg.argv.max < 75, $.confirm({
				question: 'Max quality you specified can greatly reduce the quality of images. Continue (y)?',
				input: '_key:y'
			})))
			.pipe($.filelog())
			.pipe($.jpegRecompress({
				min: min,
				max: max
			})())
			.on('error', function(err){
				$.displayError(err);
			})
			.pipe(gulp.dest($.generatePath('images')));
	}
};