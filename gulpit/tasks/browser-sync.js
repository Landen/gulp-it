/*

\========= GulpIt! =========/
/ lightning-fast processing \

task: browser-sync

runs server and watches for changing assets/templates

css and images update without page reloading
template and js update with page reloading

for wordpress, magento, custom and static

dependencies:
gulp
browser-sync

*/

module.exports = function (gulp, $) {

	var cfg = $.config;

	var syncArray = function(){
		var result = [],
			cms = cfg.cms || 'static',
			reload = cfg.browserSync.reload,
			assets = ['css', 'js', 'images', 'tpl'],
			subArray,
			assetsPath;

		switch(cms) {
			case 'magento':
				assetsPath = {
					'css': 'skin/frontend/' + cms.package + '/' + cms.theme + '/' + cfg.assets.styles.dest + '**/*.css',
					'js': 'skin/frontend/' + cms.package + '/' + cms.theme + '/js/**/*.js',
					'images': 'skin/frontend/' + cms.package + '/' + cms.theme + '/' + cfg.assets.images.src + '**/*.{jpg,png}',
					'tpl': [
						'app/design/frontend/' + cms.package + '/' + cms.theme + '/template/**/*.phtml',
						'app/design/frontend/' + cms.package + '/' + cms.theme + '/layout/**/*.xml'
					]
				};
				break;
			case 'wordpress':
				assetsPath = {
					'css': 'wp-content/themes/' + cms.theme + '/' + cfg.assets.styles.dest + '**/*.css',
					'js': 'wp-content/themes/' + cms.theme + '/js/**/*.js',
					'images': 'wp-content/themes/' + cms.theme + '/' + cfg.assets.images.src + '**/*.{jpg,png}',
					'tpl': 'wp-content/themes/' + cms.theme + '/**/*.php'
				};
				break;
			case 'custom':
				assetsPath = {
					'css': cms.assets + cfg.assets.styles.dest + '**/*.css',
					'js': cms.assets + 'js/**/*.js',
					'images': cms.assets + cfg.assets.images.src + '**/*.{jpg,png}',
					'tpl': cms.views + '**/*.' + cms.views_ext
				};
				break;
			case 'static':
				assetsPath = {
					'css': cfg.assets.styles.dest + '**/*.css',
					'js': 'js/**/*.js',
					'images': cfg.assets.images.src + '**/*.{jpg,png,gif}',
					'tpl': '**/*.{html,php}'
				};
				break;
		}
		for (asset in reload) {

			if (!reload[asset]) continue;

			if (typeof(assetsPath[asset]) === 'string') {
				result.push(assetsPath[asset]);
			} else if ($.isArray(assetsPath[asset])) {
				subArray = assetsPath[asset];
				for (var j = 0; j < subArray.length; j++) result.push(subArray[j]);
			}
		}
		console.log('Browser-Sync is wathing these files:');
		for (var i = 0; i < result.length; i++) {
			console.log('====> ' + result[i]);
		};
		return result;
	}

	return function(){
		var settings = {
			logPrefix: cfg.project,
			proxy: cfg.host,
			open: 'external',
			port: cfg.browserSync.port || 3000,
			notify: cfg.browserSync.notify || true,
			ghostMode: {
				clicks: false,
				forms: false,
				scroll: false
			}
		};
		$.browserSync.init(syncArray(), settings);
	}
};