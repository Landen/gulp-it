/*

\========= GulpIt! =========/
/ lightning-fast processing \

task: twig

dependencies:
gulp
gulp-plumber
gulp-twig
gulp-filter

*/

module.exports = function (gulp, $) {

	var cfg = $.config,
		path = path || cfg.twig.src,
		data = $.extend(cfg.twig.data, {'title': cfg.project});

	return function(){
		gulp.task('twig', function(){
			return gulp.src(path + '**/*.twig')
				.pipe($.plumber())
				.pipe($.twig({
					data: data,
					onError: function(err){$.displayError(err);}
				}))
				.pipe($.filter(['*', '!' + cfg.twig.src + '_*.twig', '!include/*.twig']))
				.pipe($.plumber.stop())
				.pipe(gulp.dest(cfg.twig.dest))
		});
	}
};