module.exports = [
	// LANGUAGE
	{
		type: 'rawlist',
		name: 'lang',
		message: 'Choose language / Выберите язык',
		choices: ['eng', 'rus'],
		default: 0
	},

	// PROJECT TITLE
	{
		name: 'title',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Project title';
				case 'rus':
					return 'Название проекта';
			}
		},
		default: 'GulpIt!'
	},

	// PROJECT DEV DOMAIN NAME
	{
		name: 'domain',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Project\'s domain name from the local server';
				case 'rus':
					return 'Доменное имя проекта из локального сервера';
			}
		},
		validate: function(val, r){
			return !!val.length;
			// var done = this.async();
			// if (!val) {
			// 	switch (r.lang){
			// 		case 'eng':
			// 			done('Project\'s domain name from the local server');
			// 		case 'rus':
			// 			done('Доменное имя проекта из локального сервера');
			// 	}
			// 	return;
			// }
			// done(true);
		}
	},

	// PREPROCESSOR
	{
		type: 'list',
		name: 'preprocessor',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Preprocessor';
				case 'rus':
					return 'Препроцессор';
			}
		},
		choices: ['sass', 'less'],
		default: 0
	},

	// PROJECT TYPE
	{
		type: 'list',
		name: 'type',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Project type';
				case 'rus':
					return 'Тип проекта';
			}
		},
		choices: ['static', 'custom', 'magento', 'wordpress'],
		default: 0
	},

	// STATIC
	{
		type: 'confirm',
		name: 'twig',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'TWIG template compilation task';
				case 'rus':
					return 'Таск для компиляции TWIG';
			}
		},
		default: false,
		when: function(r){return r.type == 'static'}
	},
	{
		name: 'twig_src',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'TWIG src folder';
				case 'rus':
					return 'Путь к папке исходников TWIG';
			}
		},
		default: 'twig/',
		when: function(r){return r.twig && r.type == 'static'}
	},
	{
		name: 'twig_dest',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'TWIG dest folder';
				case 'rus':
					return 'Путь к папке скомпилированных TWIG-темплейтов';
			}
		},
		default: '',
		when: function(r){return r.twig && r.type == 'static'}
	},
	{
		type: 'list',
		name: 'static_ext',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Files\' extension of static website pages';
				case 'rus':
					return 'Расширение файлов страниц статического сайта';
			}
		},
		choices: ['html', 'php'],
		default: 0,
		when: function(r){return !r.twig && r.type == 'static';}
	},

	// BASE DIR (IF CUSTOM)
	// {
	// 	name: 'basedir',
	// 	message: function(r){
	// 		switch (r.lang) {
	// 			case 'eng':
	// 				return 'Basedir of your project';
	// 			case 'rus':
	// 				return 'Gуть к основному каталогу проекта';
	// 		}
	// 	},
	// 	default: 'app/',
	// 	when: function(r){return r.type == 'custom';}
	// },
	// ASSETS FOLDER (IF CUSTOM)
	{
		name: 'custom_assets',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Path to the folder with assets (css, images)';
				case 'rus':
					return 'Путь к папке с ресурсами (css, images)';
			}
		},
		default: 'public/',
		when: function(r){return r.type == 'custom';}
	},
	// VIEWS FOLDER (IF CUSTOM)
	{
		name: 'custom_views',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Type a path of the folder with views';
				case 'rus':
					return 'Введите путь к папке с темплейтами';
			}
		},
		default: 'views/',
		when: function(r){return r.type == 'custom';}
	},
	{
		type: 'list',
		name: 'custom_views_ext',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Type the name of a backend template engine';
				case 'rus':
					return 'Шаблонизатор, используемый на стороне сервера';
			}
		},
		choices: ['twig', 'blade.php', 'php'],
		default: 0,
		when: function(r){return r.type == 'custom';}
	},

	// CMS PACKAGE (MAGENTO)
	{
		name: 'magento_package',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Magento package name';
				case 'rus':
					return 'Название Magento package';
			}
		},
		default: 'rwd',
		when: function(r){return r.type == 'magento';}
	},

	// CMS THEME (MAGENTO, WORDPRESS)
	{
		name: 'theme',
		message: function(r){
			var type = r.type.charAt(0).toUpperCase() + r.type.slice(1);
			switch (r.lang) {
				case 'eng':
					return 'Type your ' + type + ' project theme name';
				case 'rus':
					return 'Введите название темы ' + type + ' проекта';
			}
		},
		default: function(r){
			return r.type == 'magento' ? 'default' : 'theme_name';
		},
		when: function(r){
			return r.type == 'wordpress' || r.type == 'magento';
		}
	},

	// BROWSER-SYNC
	{
		type: 'confirm',
		name: 'browsersync',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Do you want to add Browser-Sync?';
				case 'rus':
					return 'Добавить Browser-Sync?';
			}
		},
		default: true
	},
	{
		name: 'bs_port',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return '(Browser-Sync) Type a custom port if you need';
				case 'rus':
					return '(Browser-Sync) Если требуется, введите кастомный порт для работы плагина';
			}
		},
		default: 3000,
		when: function(r){return r.browsersync;}
	},
	{
		type: 'confirm',
		name: 'bs_css',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return '(Browser-Sync) Enable css reload in browser? (Default: true)';
				case 'rus':
					return '(Browser-Sync) Включить автообновление css в браузере? (Default: true)';
			}
		},
		default: true,
		when: function(r){return r.browsersync;}
	},
	{
		type: 'confirm',
		name: 'bs_tpl',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return '(Browser-Sync) Enable templates reload in browser? (Default: false)';
				case 'rus':
					return '(Browser-Sync) Включить автообновление темплейтов в браузере? (Default: false)';
			}
		},
		default: false,
		when: function(r){return r.browsersync;}
	},
	{
		type: 'confirm',
		name: 'bs_js',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return '(Browser-Sync) Enable javascript reload in browser? (Default: false)';
				case 'rus':
					return '(Browser-Sync) Включить автообновление javascript в браузере? (Default: false)';
			}
		},
		default: false,
		when: function(r){return r.browsersync;}
	},
	{
		type: 'confirm',
		name: 'bs_images',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return '(Browser-Sync) Enable images reload in browser? (Default: false)';
				case 'rus':
					return '(Browser-Sync) Включить автообновление изображений в браузере? (Default: false)';
			}
		},
		default: false,
		when: function(r){return r.browsersync;}
	},
	{
		type: 'confirm',
		name: 'bs_notify',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return '(Browser-Sync) Enable notify? (Default: true)';
				case 'rus':
					return '(Browser-Sync) Включить оповещение о каждом успешном обновлении? (Default: false)';
			}
		},
		default: false,
		when: function(r){return r.browsersync;}
	},

	// STYLES SRC FOLDER
	{
		name: 'styles_src',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Path to the ' + r.preprocessor.toUpperCase() + ' source files folder';
				case 'rus':
					return 'Путь к папке исходников ' + r.preprocessor.toUpperCase();
			}
		},
		default: 'src/'
	},
	// STYLES DEST FOLDER
	{
		name: 'styles_dest',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Path to the CSS stylesheets folder';
				case 'rus':
					return 'Путь к папке с CSS';
			}
		},
		default: 'css/'
	},
	// IMAGES SRC FOLDER
	{
		name: 'images_src',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Path to the images SRC folder';
				case 'rus':
					return 'Путь к SRC папке с изображениями';
			}
		},
		default: 'images/'
	},
	// IMAGES DEST FOLDER
	{
		name: 'images_dest',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Path to the images DEST folder';
				case 'rus':
					return 'Путь к DEST папке с изображениями';
			}
		},
		default: 'images/'
	},

	// PNG SPRITE
	{
		type: 'confirm',
		name: 'png_sprite',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Do you need PNG-sprite task?';
				case 'rus':
					return 'Нужен ли таск для генерации PNG-спрайтов?';
			}
		},
		default: true
	},
	{
		name: 'png_sprite_name',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'PNG-sprite default name?';
				case 'rus':
					return 'Название PNG-спрайта по умолчанию?';
			}
		},
		default: 'sprite',
		when: function(r){return !!r.png_sprite;}
	},

	// SVG SPRITE
	{
		type: 'confirm',
		name: 'svg_sprite',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Do you need SVG-sprite task? (svg-sprite)';
				case 'rus':
					return 'Нужен ли таск для генерации SVG-спрайтов? (svg-sprite)';
			}
		},
		default: true
	},
	{
		name: 'svg_sprite_folder',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Folder for SVG icons (inside the images folder)';
				case 'rus':
					return 'Папка с SVG-иконками (внутри папки с изображениями)';
			}
		},
		default: 'svg-sprite',
		when: function(r){return !!r.svg_sprite;}
	},
	{
		name: 'svg_sprite_inject',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Path to the file(s) for sprite injection';
				case 'rus':
					return 'Путь к файлу для вклеивания спрайта';
			}
		},
		default: function(r){
			switch (r.type) {
				case 'magento':
					return 'template/page/html/header.phtml';
				case 'wordpress':
					return 'header.php';
				case 'custom':
					return r.custom_views + 'partials/svg.' + r.custom_views_ext;
				case 'static':
					if (r.twig) {
						return r.twig_src + 'partials/svg.twig';
					} else {
						if (r.static_ext == 'php') {
							return 'partials/svg.php';
						} else {
							return '*.html';
						}
					}
			}
		},
		when: function(r){return !!r.svg_sprite;}
	},

	// IMAGES OPTIMIZATION
	{
		type: 'confirm',
		name: 'opt_imgs',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return 'Do you need images optimization task? (png, jpg, images)';
				case 'rus':
					return 'Нужен ли таск для оптимизации изображений? (png, jpg, images)';
			}
		},
		default: true
	},

	// AUTOPREFIXER
	{
		name: 'autoprefixer_last',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return '(Autoprefixer) The count of modern browsers\' last versions, which you support (Chrome, Firefox)';
				case 'rus':
					return '(Autoprefixer) Кол-во поддерживаемых последних версий современных браузеров (Chrome, Firefox)';
			}
		},
		default: '2'
	},
	{
		name: 'autoprefixer_ios',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return '(Autoprefixer) The lowest iOS version, which you support';
				case 'rus':
					return '(Autoprefixer) iOS версия, которую поддерживаете';
			}
		},
		default: '7'
	},
	{
		name: 'autoprefixer_android',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return '(Autoprefixer) The lowest Android version, which you support';
				case 'rus':
					return '(Autoprefixer) Android версия, которую поддерживаете';
			}
		},
		default: '5'
	},
	{
		name: 'autoprefixer_ie',
		message: function(r){
			switch (r.lang) {
				case 'eng':
					return '(Autoprefixer) The lowest Internet Explorer version you support';
				case 'rus':
					return '(Autoprefixer) Нижняя версия Internet Explorer, которую поддерживаете';
			}
		},
		default: '11'
	},
]