module.exports = {
	"name": "GulpIt!",
	"version": "0.0.1",
	"description": "Unified package for a frontend development of Magento, Wordpress, custom projects and static markup",
	"devDependencies": {
		"gulp": "^3.9.1",
		"gulp-concat": "^2.6.0",
		"gulp-if": "^2.0.0",
		"gulp-load-plugins": "^1.0.0",
		"gulp-plumber": "^1.1.0",
		"gulp-sourcemaps": "^1.6.0",
		"gulp-uglify": "^1.5.3",
		"gulp-watch": "^4.3.5",
		"yargs": "^3.29.0"
	},
	"author": {
		"name": "Landen",
		"email": "dukhevych@gmail.com",
		"url": "https://landen.bitbucket.org/gulp-it"
	},
	"repository": {
		"type": "git",
		"url": "https://bitbucket.org/Landen/gulp-it"
	},
	"engines": {
		"node": "=>4.4.5"
	},
	"license": "MIT"
}