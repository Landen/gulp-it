module.exports = {
	'project': 'GulpIt!',
	'preprocessor': 'sass',
	'type': 'static',
	'host': null,
	'browsers': [],
	'assets': {
		'styles' : {
			'src' : 'src/',
			'dest': 'css/',
			'import': 'imports/',
			'include': [
				'imports',
				'blocks',
				'partials',
				'vendor',
				'override',
				'module',
				'mixin',
				'layout',
				'function',
				'core',
				'content'
			]
		},
		'images': {
			'src': 'images/',
			'dest': 'images/',
		}
	}
};