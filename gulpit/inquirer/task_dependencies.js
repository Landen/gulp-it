module.exports = {
	"browser-sync": {
		"gulp": "^3.9.1",
		"browser-sync": "^2.11.1"
	},
	"svg-sprite": {
		"gulp": "^3.9.1",
		"gulp-inject": "^3.0.0",
		"gulp-filelog": "^0.4.1",
		"gulp-cheerio": "^0.6.2",
		"gulp-svgmin": "^1.2.0",
		"gulp-svgstore": "^5.0.5"
	},
	"sass": {
		"gulp": "^3.9.1",
		"gulp-sass": "^2.2.0",
		"gulp-plumber": "^1.1.0",
		"gulp-cached": "^1.1.0",
		"gulp-progeny": "0.1.2",
		"gulp-if": "^2.0.0",
		"gulp-sourcemaps": "^1.6.0",
		"gulp-autoprefixer": "^3.1.0",
		"gulp-clean-css": "^2.0.4"
	},
	"less": {
		"gulp": "^3.9.1",
		"gulp-plumber": "^1.1.0",
		"gulp-cached": "^1.1.0",
		"gulp-progeny": "0.1.2",
		"gulp-filter": "^3.0.1",
		"gulp-if": "^2.0.0",
		"gulp-less": "^3.0.3",
		"gulp-sourcemaps": "^1.6.0",
		"gulp-autoprefixer": "^3.1.0",
		"gulp-clean-css": "^2.0.4"
	},
	"img": {
		"gulp": "^3.9.1",
		"yargs": "^3.29.0",
		"gulp-filelog": "^0.4.1",
		"gulp-if": "^2.0.0",
		"gulp-confirm": "^1.0.4",
		"imagemin-jpeg-recompress": "^4.3.0",
		"gulp-imagemin": "^2.3.0",
		"imagemin-pngquant": "^4.2.0"
	},
	"png-sprite": {
		"gulp": "^3.9.1",
		"gulp-imagemin": "^2.3.0",
		"imagemin-pngquant": "^4.2.0",
		"gulp.spritesmith": "^4.2.1"
	},
	"twig": {
		"gulp": "^3.9.1",
		"gulp-plumber": "^1.1.0",
		"gulp-twig": "^0.5.0",
		"gulp-filter": "^3.0.1",
		"gulp-html-prettify": "0.4.0"
	}
	// "svg": {
	// 	"gulp": "^3.9.1",
	// 	"gulp-svgmin": "^1.2.0"
	// },
}