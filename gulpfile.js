// Declarations
var gulp = require('gulp'),
	gulpLoadPlugins = require('gulp-load-plugins'),
	$ = gulpLoadPlugins({
		pattern: ['*'],
		rename: {
			'imagemin-jpeg-recompress' :'jpegRecompress',
			'imagemin-pngquant' :'pngquant',
			'gulp-if': '_if',
			'browser-sync': 'browserSync'
		}
	}),
	argv = require('yargs').argv;

var type, styles, images, preprocessor, cms, defaultTasks = [];

$.config = require('./gulpit/config.json');
$.extend = require('util')._extend,
type = $.config.type;
styles = $.config.assets.styles;
images = $.config.assets.images;
preprocessor = $.config.preprocessor;
cms = type !== 'static' ? $.config.cms : false;

$.config.argv = argv; // Arguments, passed through command line

$.extension = preprocessor === 'sass' ? 'scss' : preprocessor; // extension of styles files

// Generates the path to src files - depends on cms type and theme name (if exists)
$.generatePath = function(type, dest) {
	var path, dir;

	dir = $.config.assets[type][!dest ? 'src' : 'dest'];

	// paths, based on project type
	switch ($.config.cms) {
		case "magento":
			path = 'skin/frontend/' + cms.package + '/' + cms.theme + '/' + dir;
			// if (type !== 'theme') path += dir;
			break;
		case "wordpress":
			path = 'wp-content/themes/' + cms.theme + '/' + dir;
			// if (type !== 'theme') path += dir;
			break;
		case "custom":
			path = cms.assets + dir;
			// if (type !== 'theme') path += dir;
			break;
		default:
			path = '' + dir;
			// if (type !== 'theme') path += dir;
			break;
	}
	return path;
}

$.displayError = function(error) {
	var errorString = '[' + error.plugin + ']';
	errorString += ' ' + error.message.replace("\n",'');
	if(error.fileName)
		errorString += ' in ' + error.fileName;
	if(error.lineNumber)
		errorString += ' on line ' + error.lineNumber;
	console.error(errorString);
}

gulp.task(preprocessor, getTask(preprocessor));

if ($.pngquant) gulp.task('png', getTask('png'));

if ($.jpegRecompress) gulp.task('jpg', getTask('jpg'));
if ($.pngquant && $.jpegRecompress) gulp.task('images', ['png', 'jpg']);
if ($.browserSync) {
	gulp.task('browser-sync', getTask('browser-sync'));
	defaultTasks.push('browser-sync');
}
if ($.spritesmith) gulp.task('png-sprite', getTask('png-sprite'));
if ($.svgstore) gulp.task('svg-sprite', getTask('svg-sprite'));
if ($.twig) gulp.task('twig', getTask('twig'));

gulp.task('styles-watch', [preprocessor], function(){
	gulp.watch($.generatePath('styles') + '**/*.' + $.extension, [preprocessor])
	.on('change', function(evt) {
		console.log(
			'[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
		);
	})
});

defaultTasks.push('styles-watch');

if ($.twig) {
	gulp.task('twig-watch', ['twig'], function(){
		gulp.watch($.config.twig.src + '**/*.twig', ['twig'])
		.on('change', function(evt) {
			console.log(
				'[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
			);
		})
	});
	defaultTasks.push('twig-watch');
}


// default task: runs preprocessor and browser-sync (if exist)
gulp.task('default', defaultTasks);

function getTask(task) {return require('./gulpit/tasks/' + task)(gulp, $);}