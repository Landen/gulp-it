# gulp [default task]
# gulp default [default task]
# gulp sprite [sprite task]
# gulp sprite --name=new_sprite [sprite task with custom sprite name]
# gulp styles [styles task]
# gulp styles --production [styles task with minify and without sourcemaps]
# gulp jpg [jpg optimization - from images/temp to images/]
# gulp jpg --folder=temp --min=65 --max=75 [jpg optimization task with args - defaults]
# gulp jpg --force [jpg optimization - full overwrite mode]
# gulp png [png optimization task]
# gulp svg-sprite [svg sprite generation task]
# gulp images [png + jpg tasks]


# GulpIt!
##### lightning-fast processing
# Guide:

1. ### **Install**:

 * Node.js - [download](https://nodejs.org/)
 * Python - [download](https://www.python.org/downloads/)*
 * Microsoft Visual Studio C++ 2012 Express version - [download](http://go.microsoft.com/?linkid=9816758)*

> \* Some npm plugins need node-gyp to be installed. However, node-gyp has it's own dependencies ([from github page](https://github.com/TooTallNate/node-gyp#installation))

2. In your cmd:

 * ```npm install -g node-gyp```
 * ```npm install -g gulp```

3. Copy these items to you project root folder:

 * `gulpit/`
 * `package.json`
 * `gulpfile.js`
 * `gulp-init.js`
 * `gulp-init.bat`
 * `.csscomb.json`

4. Fill in your config to `gulp/config.json`

 * **`project`** - your project name (string). Default: `'GulpIt'`;
 * **`preprocessor`**: - less/sass (string). Default: `'sass'`;
 * **`production`** - boolean. If `true` - enables minification, combination of media queries and disables sourcemaps. Default: `false`;
 * **`cms`**: static/custom/wordpress/magento (string). Default: `'static'`;
 * **`browsers`**: config for autoprefixer (array). Default: `['last 2 version', 'ie 10', 'ios 6', 'android 4']`;
 * **`browserSync`**: config for browserSync task (object):
    * **`enable`**: boolean. Default: true;
    * **`port`**: integer. Default: false;
    * **`options`**: config for browserSync watching options (object);
        * **`css/template/js/images Reload` options**: boolean. Default: `true, false, false, false`;
 * **`assets`**: object for configuring your paths (styles/images):
    * **`cms_config`**: config for your cms:
        * **Magento** - package and theme name;
        * **Wordpress** - only theme name;
        * custom - 4 options (skin url, templates url, inject_folder and inject_file - for svg sprite).

5. Run `npm install` inside your project root folder.
6. Run `gulp` inside your project root folder. If you need to run separate task, you can start new command line and pass `gulp task_name`.

# Tasks (*every task file has it's description inside*):
1. **default** - preprocessor + browserSync.
1. **less/sass** (*sass - gulp-sass (based on node-sass/libsass), less - gulp-less*);
2. **browserSync** - task for auto-refreshing browser page while working on css, templates, images or js. CSS and images update without full reload of page, others - nope.
3. **images** - task for optimizing images. It compresses and overwrites png-files, but jpg files must be inside `images/temp` folder. Combination of two separate tasks: **jpg** and **png**.
4. **sprite** - task for generating png-sprite.
5. **svgstore** - task for generating svg-sprite, which would be injected into header.phtml(

## Example of `config.json`:

                {
                    "project": "test project",
                    "preprocessor": "sass",
                    "production": false,
                    "cms": "wordpress",
                    "browsers": [
                        "last 2 version",
                        "ie 10",
                        "ios 6",
                        "android 4"
                    ],
                    "browserSync": {
                        "enable": true,
                        "port": false,
                        "options": {
                            "cssReload": true,
                            "templateReload": false,
                            "jsReload": false,
                            "imagesReload": false
                        }
                    },
                    "assets": {
                        "styles" : {
                            "src" : "src/",
                            "files": "src/**/*.",
                            "dest": "css/",
                            "include": [
                                "imports",
                                "blocks",
                                "partials"
                            ]
                        },
                        "images": {
                            "src": "images/"
                        },
                        "cms_config": {
                            "magento": {
                                "package": "package_name",
                                "theme": "default"
                            },
                            "wordpress": {
                                "theme": "themename"
                            },
                            "custom": {
                                "skin": "./web/assets/",
                                "template": "../src/views/**/*.twig",
                                "inject_folder": "../src/views/layout/",
                                "inject_file": "layout.twig"
                            }
                        }
                    }
                }