try {
	var inquirer = require('inquirer');
} catch(e) {
	console.error(e.message);
	console.error("Inquirer is probably not found. Try running `npm install inquirer` inside your project folder.");
	process.exit(e.code);
}
var extend = require('util')._extend,
	fs = require('fs'),
	questions = require('./gulpit/inquirer/questions'),
	packageJSON = require('./gulpit/inquirer/package_template'),
	config = require('./gulpit/inquirer/config_template'),
	dependencies = require('./gulpit/inquirer/task_dependencies');

inquirer.prompt(questions, function(r){

	/**
	* PROJECT TITLE (1q)
	* You can see it in gulp console
	*/
	config.project = r.title;

	/**
	* LOCAL DOMAIN NAME (1q)
	* For example, domain name from the OpenServer
	*/
	config.host = r.domain;

	/**
	* PREPROCESSOR NAME (1q)
	* 2 options: sass, less
	* injecting dependencies of the PREPROCESSOR task (extends package.json)
	*/
	config.preprocessor = r.preprocessor;
	packageJSON.devDependencies = extend(packageJSON.devDependencies, dependencies[r.preprocessor]);

	/**
	* PROJECT TYPE (1q)
	* 4 options: magento, wordpress, custom, static
	*/
	config.type = r.type;

	/**
	* TWIG (3q)
	* injecting dependencies of the TWIG compilation task (extends package.json)
	*/
	if (r.twig) {
		packageJSON.devDependencies = extend(packageJSON.devDependencies, dependencies['twig']);
		config.twig = {};
		config.twig.src = r.twig_src;
		config.twig.dest = r.twig_dest;
		config.twig.data = {};
	}

	/**
	* STATIC FILES EXTENSION (IF !TWIG) (1q)
	* 2 options: html, php
	*/
	if (r.static_ext) config.tpl = r.static_ext;

	/**
	* BASEDIR (IF CUSTOM) (1q)
	* default: 'app/'
	*/
	if (r.basedir) config.basedir = r.basedir;

	/**
	* CMS CONFIG
	*/
	if (r.type != 'static') config.cms = {};

	/**
	* CMS CONFIG (IF !STATIC)
	* PACKAGE (IF MAGENTO) (1q)
	* THEME (IF MAGENTO || WORDPRESS) (1q)
	* ASSETS (IF CUSTOM) (1q)
	* VIEWS (IF CUSTOM) (1q)
	*/
	switch(r.type) {
		case 'magento':
		case 'wordpress':
			config.cms.theme = r.theme;
		case 'magento':
			config.cms.package = r.package;
			break;
		case 'custom':
			config.cms.assets = r.custom_assets;
			config.cms.views =  r.custom_views;
			break;
	}

	/**
	* BROWSER-SYNC (7q)
	* settings: port, css, templates, js, images
	*/
	if (r.browsersync) {
		packageJSON.devDependencies = extend(packageJSON.devDependencies, dependencies['browser-sync']);
		config.browserSync = {reload: {}};
		config.browserSync.port = r.bs_port;
		config.browserSync.notify = r.bs_notify;
		config.browserSync.reload.css = r.bs_css;
		config.browserSync.reload.tpl = r.bs_tpl;
		config.browserSync.reload.js = r.bs_js;
		config.browserSync.reload.images = r.bs_images;
	}

	/**
	* STYLES path validation (2q)
	* IMAGES path validation (2q)
	*/
	config.assets.styles.src = r.styles_src[r.styles_src.length - 1] == '/' ? r.styles_src : r.styles_src + '/';
	config.assets.styles.dest = r.styles_dest[r.styles_dest.length - 1] == '/' ? r.styles_dest : r.styles_dest + '/';
	config.assets.images.src = r.images_src[r.images_src.length - 1] == '/' ? r.images_src : r.images_src + '/';
	config.assets.images.dest = r.images_dest[r.images_dest.length - 1] == '/' ? r.images_dest : r.images_dest + '/';

	if (r.png_sprite || r.svg_sprite) {
		config.assets.sprite = {};
	}

	/**
	* PNG SPRITE (2q)
	* injecting dependencies of the PNG Sprite task (extends package.json)
	*/
	if (r.png_sprite) {
		config.assets.sprite.png = r.png_sprite_name;
		packageJSON.devDependencies = extend(packageJSON.devDependencies, dependencies['png-sprite'])
	}

	/**
	* SVG SPRITE (3q)
	* injecting dependencies of the SVG Sprite task (extends package.json)
	*/
	if (r.svg_sprite) {
		packageJSON.devDependencies = extend(packageJSON.devDependencies, dependencies['svg-sprite'])
		config.assets.sprite.svg = {};
		config.assets.sprite.svg.inject = {};
		config.assets.sprite.svg.folder = r.svg_sprite_folder;
	}

	/**
	* IMAGES OPTIMIZATION (PNG + JPG) (1q)
	* injecting dependencies of the IMAGES optimization task (extends package.json)
	*/
	if (r.opt_imgs) packageJSON.devDependencies = extend(packageJSON.devDependencies, dependencies['img']);

	/**
	* AUTOPREFIXER (4q)
	* browsers' settings: last versions, iOS, Android and IE
	*/
	config.browsers.push('last ' + r.autoprefixer_last + ' versions');
	config.browsers.push('iOS >= ' + r.autoprefixer_ios);
	config.browsers.push('Android >= ' + r.autoprefixer_android);
	config.browsers.push('IE >= ' + r.autoprefixer_ie);

	/**
	* Generates package.json file
	*/
	fs.writeFile('package.json', JSON.stringify(packageJSON, null, '\t'), function(err){
		if (err) throw err;
		console.log('\npackage.json has been generated');
	});

	/**
	* Generates config.json file
	*/
	fs.writeFile('gulpit/config.json', JSON.stringify(config, null, '\t'), function(err){
		if (err) throw err;
		console.log('\nconfig.json has been generated\nYou can change this settings at any time, just edit file `gulpit/config.json`');
		console.log('\nNow install all nodejs dependencies from package.json. Just run from the root of your project this command:\n\nnpm install\n\nand wait for a while :)');
	});

	setTimeout(function(){
	}, 1000);
});